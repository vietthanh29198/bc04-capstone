// light dark theme
document.getElementById("switchButton").onclick = function () {
  document.getElementById("myBody").classList.toggle("dark");
};

// open close overlay
function openOverlay() {
  document.getElementById("overlay").classList.add("open");
}

function closeOverlay() {
  document.getElementById("overlay").classList.remove("open");
}

// magnific popup
$(document).ready(function () {
  $(".popup-with-zoom-anim").magnificPopup({
    type: "inline",
    fixedContentPos: false,
    fixedBgPos: true,
    overflowY: "auto",
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    mainClass: "my-mfp-zoom-in",
  });
});
